package com.crni03.api.phonebook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class PhoneBookService {
	@Autowired
	PhoneBookRepository repository;

	public String process() {
		repository.save(new PhoneBook("Jack", "Smith", "095/ 858 896"));
		repository.save(new PhoneBook("Adam", "Johnson", "092/ 554 558"));
		repository.save(new PhoneBook("Kim", "Smith", "091/223 32 25"));
		repository.save(new PhoneBook("David", "Williams", "098/125 55 88"));
		repository.save(new PhoneBook("Peter", "Davis", "089 888 888"));
		repository.save(new PhoneBook("Michael", "Moore", "091 158 88 58"));
		return "phonebook";
	}

	public void addPhoneBook(@RequestBody PhoneBook phonebook) {
		repository.save(phonebook);

	}

	public String findAll() {
		// String result = "<html>";
		String result = "";

		for (PhoneBook pb : repository.findAll()) {
			// result += "<div>" + cust.toString() + "</div>";
			result += pb.toString() + "\\\n";
		}
		// return result + "</html>";
		return result;
	}

	public String findById(long id) {
		String result = "";
		result = repository.findOne(id).toString();
		return result;
	}

	public Long findId(long id) {
		Long result = null;
		try {
			result = repository.findOne(id).getId();
		} catch (Exception e) {
			System.out.print(e);
		}
		return result;
	}

	public String findFirstName(long id) {
		String result = "";
		result = repository.findOne(id).getFirstName();
		return result;
	}

	public String findLastName(long id) {
		String result = "";
		result = repository.findOne(id).getLastName();
		return result;
	}

	public String findPhone(long id) {
		String result = "";
		result = repository.findOne(id).getPhone();
		return result;
	}

	public void deletePhoneBook(long id) {
		repository.delete(id);
	}

	public String updatePhoneBook(PhoneBook pb, long id) {
		pb.setId(id);
		repository.save(pb);
		return findById(id);
	}

	public String findDataByLastName(String lastName) {
		String result = "";

		for (PhoneBook pb : repository.findByLastNameIgnoreCase(lastName)) {
			result += pb.toString() + "\\\n";
		}
		return result;
	}

	public String findDataByFirstName(String firstname) {
		String result = "";

		for (PhoneBook pb : repository.findByFirstNameIgnoreCase(firstname)) {
			result += pb.toString() + "\\\n";
		}
		return result;
	}

}
