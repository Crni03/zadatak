package com.crni03.api.phonebook;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import java.lang.String;
import com.crni03.api.phonebook.PhoneBook;

public interface PhoneBookRepository extends CrudRepository<PhoneBook, Long>{
	List<PhoneBook> findByLastNameIgnoreCase(String lastName);
	List<PhoneBook> findByFirstNameIgnoreCase(String firstName);
	
	
}
