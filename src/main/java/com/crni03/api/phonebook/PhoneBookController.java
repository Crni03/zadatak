package com.crni03.api.phonebook;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PhoneBookController {
	@Autowired
	PhoneBookService service;

	PhoneBook phonebook;

	@RequestMapping("/add")
	public String process(Model model) {
		String htmlDoc = service.process();
		model.addAttribute("pb", service.findAll());
		return htmlDoc;
	}

	@RequestMapping("index")
	public String index(Model model) {

		model.addAttribute("pb", service.findAll());
		return "phonebook";
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/findbyid")
	public void deletePhoneBook(@RequestParam("id") long id) {
		service.deletePhoneBook(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/findbyid")
	public String updatePhoneBook(@RequestBody PhoneBook pb, @RequestParam("id") long id) {
		return service.updatePhoneBook(pb, id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save")
	public void addPhoneBook(@RequestBody PhoneBook phonebook) {
		service.addPhoneBook(phonebook);
	}

	@RequestMapping(value = "/findall", method = RequestMethod.GET)
	public String findAll(Model model) {

		model.addAttribute("pb", service.findAll());
		return "phonebook";
	}

	@RequestMapping(value = "/findall", method = RequestMethod.POST)
	public String get(HttpServletRequest request, Model model) {
		phonebook = new PhoneBook(request.getParameter("first_name"), request.getParameter("last_name"),
				request.getParameter("phone"));
		service.addPhoneBook(phonebook);
		System.out.println("create entry: " + phonebook.getId());
		System.out.println("create entry: " + phonebook.getFirstName());
		System.out.println("create entry: " + phonebook.getLastName());
		System.out.println("create entry: " + phonebook.getPhone());

		model.addAttribute("pb", service.findAll());
		model.addAttribute("id", phonebook.getId());
		model.addAttribute("first_name", request.getParameter("first_name"));
		model.addAttribute("last_name", request.getParameter("last_name"));
		model.addAttribute("phone", request.getParameter("phone"));

		return "phonebook";
	}

	@RequestMapping("/findbyid")
	public String findById(HttpServletRequest request, Model model) {
		String sId = request.getParameter("id");
		System.out.println("findById");

		if (!sId.contentEquals("") && service.findId(Long.valueOf(sId)) != null) {
			phonebook = new PhoneBook(request.getParameter("first_name"), request.getParameter("last_name"),
					request.getParameter("phone"));

			System.out.println("phonebook entry: " + phonebook.getId());
			System.out.println("phonebook entry: " + phonebook.getFirstName());
			System.out.println("phonebook entry: " + phonebook.getLastName());
			System.out.println("phonebook entry: " + phonebook.getPhone());

			Long id = Long.decode(sId);
			String delete = request.getParameter("delete");
			String edit = request.getParameter("edit");
			if (edit != null)
				service.updatePhoneBook(phonebook, id);

			System.out.println("findById" + delete);
			System.out.println("findById" + id);
			if (delete != null) {
				service.deletePhoneBook(id);
			} else {
				model.addAttribute("id", id);
				model.addAttribute("first_name", service.findFirstName(id));
				model.addAttribute("last_name", service.findLastName(id));
				model.addAttribute("phone", service.findPhone(id));
			
			System.out.println("findById : " + service.findById(id));
			System.out.println("findById : " + service.findFirstName(id));
			System.out.println("findById : " + service.findLastName(id));
			System.out.println("findById : " + service.findPhone(id));
			}
		}
		model.addAttribute("pb", service.findAll());
		return "phonebook";
	}

	@RequestMapping("/findbyfirstname")
	public String findDataByFirstName(@RequestParam("firstname") String firstname, Model model) {
		model.addAttribute("pb", service.findDataByFirstName(firstname));
		return "phonebook";
	}

	@RequestMapping("/findbylastname")
	public String findDataByLastName(@RequestParam("lastname") String lastName, Model model) {
		model.addAttribute("pb", service.findDataByLastName(lastName));
		return "phonebook";
	}

}
