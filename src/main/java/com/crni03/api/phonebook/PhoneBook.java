package com.crni03.api.phonebook;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "phonebook")
public class PhoneBook implements Serializable {

	private static final long serialVersionUID = -3009157732242241606L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "firstname")
	private String firstName;

	@Column(name = "lastname")
	private String lastName;
	
	@Column(name = "phone")
	private String phone;

	public PhoneBook() {
	}

	public PhoneBook(String firstName, String lastName, String phone) {
		super();
		System.out.println(firstName);
		System.out.println(lastName);
		System.out.println(phone);

		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
	}

	@Override
	public String toString() {
		return String.format("Entry[id=%d, firstName='%s', lastName='%s', phone='%s']", id, firstName, lastName, phone);
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
}
